package src;

import java.awt.*;

public abstract class Item {

    protected KeyManager keyManager;
    protected Handler handler;
    private int id;

    public Item(Handler handler, KeyManager keyManager, int id) {
        this.keyManager = keyManager;
        this.handler = handler;
        this.id = id;
    }

    public abstract void tick();

    public abstract void render(Graphics graphics);
}
