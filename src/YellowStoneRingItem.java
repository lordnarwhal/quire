package src;

import java.awt.*;

public class YellowStoneRingItem extends Item {
    private int darknessLevel = 18;
    private int chargeLevel = 0;
    private boolean isCharging = false;
    private long chargeTimer, dischargeTimer;
    private long chargeThreshold = 1000000000L, dischargeThreshold = 5000000000L;     //nanoseconds

    public YellowStoneRingItem(Handler handler, KeyManager keyManager, int id) {
        super(handler, keyManager, id);
    }

    @Override
    public void tick() {
        if (keyManager.x) {
            charge();
        } else {
            isCharging = false;
        }
        if (isCharging) {
            charge();
        } else {
            if (System.nanoTime() - dischargeTimer >= dischargeThreshold && darknessLevel < 18) {
                discharge();
            }
        }
    }

    @Override
    public void render(Graphics graphics) {
        drawVision(graphics);
    }

    private void charge() {
        if (!isCharging) {
            chargeTimer = System.nanoTime();
        }
        if (chargeLevel == 18 || keyManager.up || keyManager.left || keyManager.down || keyManager.right) {
            isCharging = false;
        } else {
            isCharging = true;
            if (System.nanoTime() - chargeTimer >= chargeThreshold) {
                chargeLevel++;
                darknessLevel--;
                chargeTimer = System.nanoTime();
                dischargeTimer = System.nanoTime();
            }
        }
    }

    private void discharge() {
        chargeLevel--;
        darknessLevel++;
        dischargeTimer = System.nanoTime();
    }

    private void drawVision(Graphics graphics) {
        int xOffsetCamera = (int) handler.getGameCamera().getxOffsetCamera();
        int yOffsetCamera = (int) handler.getGameCamera().getyOffsetCamera();
        int xOffset = (int) handler.getGameCamera().getxOffset();
        int yOffset = (int) handler.getGameCamera().getyOffset();
        int xDrawFrom = -handler.getWidth();
        int yDrawFrom = -handler.getHeight();
        int xScale = handler.getWidth() * 3;
        int yScale = handler.getHeight() * 3;

        //we need to check the 4 cardinal directions and the diagonals to figure out where to center the vision
        if (xOffsetCamera < 0) {
            xDrawFrom += xOffsetCamera;
        } else if (xOffsetCamera > xOffset) {
            xDrawFrom += xOffsetCamera - xOffset;
        }
        if (yOffsetCamera < 0) {
            yDrawFrom += yOffsetCamera;
        } else if (yOffsetCamera > yOffset) {
            yDrawFrom += yOffsetCamera - yOffset;
        }
        graphics.drawImage(Assets.tunnelVision[darknessLevel], xDrawFrom, yDrawFrom, xScale, yScale, null);
    }

    public int getDarknessLevel() {
        return darknessLevel;
    }

    public void setDarknessLevel(int darknessLevel) {
        this.darknessLevel = darknessLevel;
    }
}
