package src;

import java.awt.*;

public class SurvivalStatus {

    public static int darknessLevel = 0;
    public static int currentTemperature = 5;
    public static int currentTime = 3;
    public static int currentFear = 0;
    private Handler handler;
    private KeyManager keyManager;
    private Player player;

    public SurvivalStatus(Handler handler) {
        this.handler = handler;
    }

    public void tick() {
        keyManager = handler.getKeyManager();
        if (keyManager.one) {
            if (!keyManager.isStillHolding1()) {
                keyManager.setStillHolding1(true);
                currentTime += 1;
                if (currentTime >= 7) {
                    currentTime = 0;
                }
            }
        }

        if (keyManager.two) {
            if (!keyManager.isStillHolding2()) {
                keyManager.setStillHolding2(true);
                currentTemperature += 1;
                if (currentTemperature >= 12) {
                    currentTemperature = 0;
                }
            }
        }

    }

    public void render(Graphics graphics) {
        player = handler.getWorld().getEntityManager().getPlayer();
        graphics.drawImage(Assets.currentTime[currentTime], handler.getWidth() / 2 - 48, 0, 96, 96, null);
    }

    public static int getCurrentTemperature() {
        return currentTemperature;
    }

    public static int getCurrentTime() {
        return currentTime;
    }

    public int getCurrentFear() {
        return currentFear;
    }
}
