package src;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Assets {
	
	private static final int width = 32, height = 32;
	public static final int textboxWidth = 228, textboxHeight = 96;
	
	public static BufferedImage dirt, grass, stone, tree, water,
								playerDownNormal, playerUpNormal, playerLeftNormal, playerRightNormal;
	public static BufferedImage temperatureHot, temperatureCold, visionWideMax;
	public static BufferedImage ringOverworld;
	public static BufferedImage textbox, textbox_player;
	public static BufferedImage[] player_down, player_up, player_left, player_right;
	public static BufferedImage[] currentTime;
	public static BufferedImage[] tunnelVision;
	public static Font philosopher;

	public static void init(){
		try {
		    //create the font to use. Specify the size!
		    philosopher = Font.createFont(Font.TRUETYPE_FONT, new File("res/fonts/Philosopher.ttf")).deriveFont(72f);
		    GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		    //register the font
		    ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("res/fonts/Philosopher.ttf")));
		} catch (IOException | FontFormatException e) {
		    e.printStackTrace();
		}

		SpriteSheet sheet = new SpriteSheet(ImageLoader.loadImage("/res/textures/sheet.png"));
		SpriteSheet playerSheet = new SpriteSheet(ImageLoader.loadImage("/res/textures/newcharactersheet.png"));
		SpriteSheet daynightSheet = new SpriteSheet(ImageLoader.loadImage("/res/textures/daynightHud.png"));
		
		textbox_player = ImageLoader.loadImage("/res/textures/tb1.png");
		textbox = ImageLoader.loadImage("/res/textures/tb.png");

		temperatureHot = ImageLoader.loadImage("/res/textures/temperature-hot.png");
		temperatureCold = ImageLoader.loadImage("/res/textures/temperature-cold.png");

		ringOverworld = ImageLoader.loadImage("/res/textures/ring-overworld.png");

		currentTime		= new BufferedImage[7];
		currentTime[0] 	= daynightSheet.crop(0, 0, 48, 48);
		currentTime[1] 	= daynightSheet.crop(48, 0, 48, 48);
		currentTime[2] 	= daynightSheet.crop(96, 0, 48, 48);
		currentTime[3] 	= daynightSheet.crop(144, 0, 48, 48);
		currentTime[4] 	= daynightSheet.crop(0, 48, 48, 48);
		currentTime[5] 	= daynightSheet.crop(48, 48, 48, 48);
		currentTime[6] 	= daynightSheet.crop(96, 48, 48, 48);

		tunnelVision = new BufferedImage[19];
		tunnelVision[0] = ImageLoader.loadImage("/res/textures/vision-110-35.png");
		tunnelVision[1] = ImageLoader.loadImage("/res/textures/vision-105-35.png");
		tunnelVision[2] = ImageLoader.loadImage("/res/textures/vision-100-35.png");
		tunnelVision[3] = ImageLoader.loadImage("/res/textures/vision-95-35.png");
		tunnelVision[4] = ImageLoader.loadImage("/res/textures/vision-90-35.png");
		tunnelVision[5] = ImageLoader.loadImage("/res/textures/vision-85-35.png");
		tunnelVision[6] = ImageLoader.loadImage("/res/textures/vision-80-35.png");
		tunnelVision[7] = ImageLoader.loadImage("/res/textures/vision-75-35.png");
		tunnelVision[8] = ImageLoader.loadImage("/res/textures/vision-70-35.png");
		tunnelVision[9] = ImageLoader.loadImage("/res/textures/vision-65-35.png");
		tunnelVision[10] = ImageLoader.loadImage("/res/textures/vision-60-35.png");
		tunnelVision[11] = ImageLoader.loadImage("/res/textures/vision-55-35.png");
		tunnelVision[12] = ImageLoader.loadImage("/res/textures/vision-50-35.png");
		tunnelVision[13] = ImageLoader.loadImage("/res/textures/vision-45-35.png");
		tunnelVision[14] = ImageLoader.loadImage("/res/textures/vision-40-35.png");
		tunnelVision[15] = ImageLoader.loadImage("/res/textures/vision-35-35.png");
		tunnelVision[16] = ImageLoader.loadImage("/res/textures/vision-30-35.png");
		tunnelVision[17] = ImageLoader.loadImage("/res/textures/vision-25-35.png");
		tunnelVision[18] = ImageLoader.loadImage("/res/textures/vision-20-35.png");

		player_down = new BufferedImage[4]; 		//4 = frame count
		player_up = new BufferedImage[4];
		player_left = new BufferedImage[6];
		player_right = new BufferedImage[6];
		
		player_down[0] = playerSheet.crop(0, 0, width, height);
		player_down[1] = playerSheet.crop(width, 0, width, height);
		player_down[2] = playerSheet.crop(width*2, 0, width, height);
		player_down[3] = playerSheet.crop(width*3, 0, width, height);
		
		player_up[0] = playerSheet.crop(0, height, width, height);
		player_up[1] = playerSheet.crop(width, height, width, height);
		player_up[2] = playerSheet.crop(width*2, height, width, height);
		player_up[3] = playerSheet.crop(width*3, height, width, height);
		
		player_left[0] = playerSheet.crop(0, height*2, width, height);
		player_left[1] = playerSheet.crop(width, height*2, width, height);
		player_left[2] = playerSheet.crop(width*2, height*2, width, height);
		player_left[3] = playerSheet.crop(width*3, height*2, width, height);
		player_left[4] = playerSheet.crop(width*4, height*2, width, height);
		player_left[5] = playerSheet.crop(width*5, height*2, width, height);
		
		player_right[0] = playerSheet.crop(0, height*3, width, height);
		player_right[1] = playerSheet.crop(width, height*3, width, height);
		player_right[2] = playerSheet.crop(width*2, height*3, width, height);
		player_right[3] = playerSheet.crop(width*3, height*3, width, height);
		player_right[4] = playerSheet.crop(width*4, height*3, width, height);
		player_right[5] = playerSheet.crop(width*5, height*3, width, height);
		
		playerDownNormal = player_down[0];
		playerLeftNormal = player_left[5];
		playerRightNormal = player_right[5];
		playerUpNormal = player_up[0];
				
		dirt = sheet.crop(0,0,width,height);
		grass = sheet.crop(width, 0, width, height);
		stone = sheet.crop(width*2, 0, width, height);
		tree = sheet.crop(width*3, 0, width, height);
		water = sheet.crop(0, height*2, width, height);
	}
	public static int getTextboxHeight()
	{
		return textboxHeight;
	}
	
	public static int getTextboxWidth()
	{
		return textboxWidth;
	}
}
